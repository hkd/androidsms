package com.app.koodateur.dbb.entities;

import java.util.Date;

/**
 * Created by koodateur on 06/05/2018.
 */

public class SmsData {
    private int _id;
    private String message;
    private String contact;
    private String daterecu;
    private String type;

    public SmsData(){}
    public SmsData(int _id, String message, String contact, String daterecu, String type) {
        this._id = _id;
        this.message = message;
        this.contact = contact;
        this.daterecu = daterecu;
        this.type = type;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getDaterecu() {
        Long da=Long.parseLong(daterecu);
        Date daa=new Date(da);
        return daa.toString();
    }

    public void setDaterecu(String daterecu) {

        this.daterecu = daterecu;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        Long da=Long.parseLong(daterecu);
        Date daa=new Date(da);
        return "contact:"+contact+"\n"+
                "message:"+message+"\n"+
                "_id:"+_id+"\n"+
                "date : "+daa+"\n";
    }
}
