package com.app.koodateur.dbb.dao;

/**
 * Created by koodateur on 06/05/2018.
 */

public final class DataContract {
    DataContract(){}
    public static class DataEntryContact{
        public static String TABLE_NAME="Message";
        public static String _ID="_id";
        public static String COLUMN_Message="message";
        public static String COLUMN_Type="type";
        public static String COLUMN_contact="contact";
        public static String COLUMN_daterecu="daterecu";
        public static String DATABASE_NAME="iCloudMessage";
        public static Integer VERSION=1;
    }

    public static String getSqlCreateEntries() {
        return SQL_CREATE_ENTRIES;
    }

    public static String getSqlDeleteEntries() {
        return SQL_DELETE_ENTRIES;
    }

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE "+DataEntryContact.TABLE_NAME+"("+
                    DataEntryContact._ID+" INTEGER PRIMARY KEY,"+
                    DataEntryContact.COLUMN_Message+" TEXT,"+
                    DataEntryContact.COLUMN_contact+" TEXT,"+
                    DataEntryContact.COLUMN_Type+" TEXT,"+
                    DataEntryContact.COLUMN_daterecu+ " TEXT)";
    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + DataEntryContact.TABLE_NAME;
}
