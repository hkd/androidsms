package com.app.koodateur.dbb.dao;

/**
 * Created by koodateur on 06/05/2018.
 */

    import java.util.ArrayList;

    import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

    import com.app.koodateur.dbb.entities.SmsData;

public class DataBase extends SQLiteOpenHelper {

    public DataBase(Context context) {
        super(context, DataContract.DataEntryContact.DATABASE_NAME, null,  DataContract.DataEntryContact.VERSION);
    }
        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DataContract.getSqlCreateEntries());
        }

        @Override
        public void onOpen(SQLiteDatabase db) {
            super.onOpen(db);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(DataContract.getSqlDeleteEntries());
            onCreate(db);

        }

        public void addMessageData(SmsData sms) {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(DataContract.DataEntryContact.COLUMN_Message,sms.getMessage());
            values.put(DataContract.DataEntryContact.COLUMN_contact,sms.getContact());
            values.put(DataContract.DataEntryContact.COLUMN_daterecu,sms.getDaterecu());
            values.put(DataContract.DataEntryContact.COLUMN_Type,sms.getType());
            values.put(DataContract.DataEntryContact._ID,sms.get_id());
            db.insertOrThrow(DataContract.DataEntryContact.TABLE_NAME, null, values);
            db.close();
        }
        public ArrayList<SmsData> getAllMessageData(String contact){

            ArrayList<SmsData>sms=new ArrayList<>();
            String requete="SELECT * FROM "+
                    DataContract.DataEntryContact.TABLE_NAME+
                    " order by "+ DataContract.DataEntryContact.COLUMN_daterecu+" ASC";
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(requete, null);
            if (cursor.moveToFirst()) {
                do {
                    SmsData smsItem = new SmsData();
                    smsItem.set_id(cursor.getInt(cursor.getColumnIndex(DataContract.DataEntryContact._ID)));
                    smsItem.setMessage(cursor.getString(cursor.getColumnIndex(DataContract.DataEntryContact.COLUMN_Message)));
                    smsItem.setContact(cursor.getString(cursor.getColumnIndex(DataContract.DataEntryContact.COLUMN_contact)));
                    smsItem.setType(cursor.getString(cursor.getColumnIndex(DataContract.DataEntryContact.COLUMN_Type)));
                    smsItem.setDaterecu(cursor.getString(cursor.getColumnIndex(DataContract.DataEntryContact.COLUMN_daterecu)));
                    sms.add(smsItem);
                } while (cursor.moveToNext());
            }
            return sms;
        }


        public boolean deleteContact(SmsData sms) {
            SQLiteDatabase db = this.getWritableDatabase();
            Integer res=db.delete(DataContract.DataEntryContact.TABLE_NAME,DataContract.DataEntryContact._ID+ " = ?",
                    new String[] { String.valueOf(sms.get_id()) });
            db.close();
            if(res>0){return true;}
            return false;
        }
    }

